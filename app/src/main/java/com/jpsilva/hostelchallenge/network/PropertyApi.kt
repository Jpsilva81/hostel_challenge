package com.jpsilva.hostelchallenge.network


import com.jpsilva.hostelchallenge.model.Property
import io.reactivex.Observable
import retrofit2.http.GET

/**
 * The interface which provides methods to get result of webservices
 */
interface PropertyApi {
    /**
     * Get the list of the pots from the API
     */
    @GET("/ruimendesM/bf8d095f2e92da94938810b8a8187c21/raw/70b112f88e803bf0f101f2c823a186f3d076d9e6")
    fun getProperties(): Observable<Property>
}
package com.jpsilva.hostelchallenge.utils

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Observer
import android.databinding.BindingAdapter
import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso

@BindingAdapter("mutableVisibility")
fun setMutableVisibility(view: View, visibility: MutableLiveData<Int>?) {
    val parentActivity: AppCompatActivity? = view.getParentActivity()
    if(parentActivity != null && visibility != null) {
        visibility.observe(parentActivity, Observer { value -> view.visibility = value?:View.VISIBLE})
    }
}
@BindingAdapter("mutableText")
fun setMutableText(view: TextView, text: MutableLiveData<String>?) {
    val parentActivity:AppCompatActivity? = view.getParentActivity()
    if(parentActivity != null && text != null) {
        text.observe(parentActivity, Observer { value -> view.text = value?:""})
    }
}
@BindingAdapter("adapter")
fun setAdapter(view: RecyclerView, adapter: RecyclerView.Adapter<*>) {
    view.adapter = adapter
}
@BindingAdapter("image_url")
fun setImageUrl(imageView: ImageView, url: MutableLiveData<String>?) {
    Picasso.get()
            .load(url!!.value)
            .into(imageView)
}
@BindingAdapter("text_color")
fun setFont(textView : TextView, rating : MutableLiveData<String>) {
    when {
        rating.value!!.toDouble() > 8 -> textView.setTextColor(Color.GREEN)
        rating.value!!.toDouble() > 6 -> textView.setTextColor(Color.YELLOW)
        else -> textView.setTextColor(Color.RED)
    }
}
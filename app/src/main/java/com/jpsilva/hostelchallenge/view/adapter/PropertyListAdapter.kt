package com.jpsilva.hostelchallenge.view.adapter

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.jpsilva.hostelchallenge.R
import com.jpsilva.hostelchallenge.databinding.ItemPropertyBinding
import com.jpsilva.hostelchallenge.model.Properties
import com.jpsilva.hostelchallenge.viewmodel.PropertyViewModel

class PropertyListAdapter: RecyclerView.Adapter<PropertyListAdapter.ViewHolder>() {
    private lateinit var postList:List<Properties>

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PropertyListAdapter.ViewHolder {
        val binding: ItemPropertyBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_property, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: PropertyListAdapter.ViewHolder, position: Int) {
        holder.bind(postList[position])
    }

    override fun getItemCount(): Int {
        return if(::postList.isInitialized) postList.size else 0
    }

    fun updatePostList(postList:List<Properties>){
        this.postList = postList
        notifyDataSetChanged()
    }

    class ViewHolder(private val binding: ItemPropertyBinding):RecyclerView.ViewHolder(binding.root){
        private val viewModel = PropertyViewModel()
        fun bind(post: Properties){
            viewModel.bind(post)
            binding.viewModel = viewModel
        }
    }
}
package com.jpsilva.hostelchallenge

import android.arch.core.executor.testing.InstantTaskExecutorRule
import com.jpsilva.hostelchallenge.utils.CONVERTER
import com.jpsilva.hostelchallenge.viewmodel.PropertyViewModel
import junit.framework.Assert.assertEquals
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito.*

class PropertyViewModelTest {

    @Rule
    @JvmField
    val instantExecutor = InstantTaskExecutorRule()

    @Test
    fun calculatePriceInEurosTest() {
        val mockViewModel = mock(PropertyViewModel::class.java)
        `when`(mockViewModel.calculatePriceInEuros("755")).thenReturn("100,00€")
        val value = (755 / CONVERTER)
        val amount = String.format("%.2f", value) + "€"
        assertEquals("100,00€", amount)
    }

    @Test
    fun getRatingTest() {
        val mockViewModel = mock(PropertyViewModel::class.java)
        `when`(mockViewModel.getRating(99)).thenReturn("9.9")
        val amount = (99f / 10).toString()
        assertEquals("9.9", amount)
    }
}
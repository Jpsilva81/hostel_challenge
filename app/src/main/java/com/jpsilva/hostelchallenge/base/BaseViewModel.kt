package com.jpsilva.hostelchallenge.base

import android.arch.lifecycle.ViewModel
import com.jpsilva.hostelchallenge.injection.DaggerViewModelInjector
import com.jpsilva.hostelchallenge.injection.ViewModelInjector
import com.jpsilva.hostelchallenge.module.NetworkModule
import com.jpsilva.hostelchallenge.viewmodel.PropertyListViewModel

abstract class BaseViewModel: ViewModel(){
    private val injector: ViewModelInjector = DaggerViewModelInjector
            .builder()
            .networkModule(NetworkModule)
            .build()

    init {
        inject()
    }

    /**
     * Injects the required dependencies
     */
    private fun inject() {
        when (this) {
            is PropertyListViewModel -> injector.inject(this)
        }
    }
}

package com.jpsilva.hostelchallenge.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.view.View
import com.jpsilva.hostelchallenge.R
import com.jpsilva.hostelchallenge.base.BaseViewModel
import com.jpsilva.hostelchallenge.model.Properties
import com.jpsilva.hostelchallenge.network.PropertyApi
import com.jpsilva.hostelchallenge.view.adapter.PropertyListAdapter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class PropertyListViewModel: BaseViewModel(){
    @Inject
    lateinit var propertyApi: PropertyApi

    private lateinit var subscription: Disposable
    val loadingVisibility: MutableLiveData<Int> = MutableLiveData()
    val errorMessage:MutableLiveData<Int> = MutableLiveData()
    val errorClickListener = View.OnClickListener { loadProperties() }
    val propertyListAdapter: PropertyListAdapter = PropertyListAdapter()

    init{
        loadProperties()
    }

    private fun loadProperties(){
        subscription = propertyApi.getProperties()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { onRetrievePropertyListStart() }
                .doOnTerminate { onRetrievePropertyListFinish() }
                .subscribe(
                        // Add result
                        { result -> onRetrievePropertyListSuccess(result.properties!!) },
                        { onRetrievePropertyListError() }
                )
    }

    private fun onRetrievePropertyListStart(){
        loadingVisibility.value = View.VISIBLE
        errorMessage.value = null
    }

    private fun onRetrievePropertyListFinish(){
        loadingVisibility.value = View.GONE
    }

    private fun onRetrievePropertyListSuccess(propertyList:List<Properties>){
        propertyListAdapter.updatePostList(propertyList)
    }

    private fun onRetrievePropertyListError(){
        errorMessage.value = R.string.error
    }

    override fun onCleared() {
        super.onCleared()
        subscription.dispose()
    }
}
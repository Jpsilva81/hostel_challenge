package com.jpsilva.hostelchallenge.viewmodel

import android.arch.lifecycle.MutableLiveData
import com.jpsilva.hostelchallenge.base.BaseViewModel
import com.jpsilva.hostelchallenge.model.Properties
import com.jpsilva.hostelchallenge.utils.CONVERTER


class PropertyViewModel: BaseViewModel() {
    private val propertyName = MutableLiveData<String>()
    private val propertyRating = MutableLiveData<String>()
    private val propertyImageUrl = MutableLiveData<String>()
    private val propertyLowestPricePerNight = MutableLiveData<String>()
    private val propertyOverview = MutableLiveData<String>()

    fun bind(prop: Properties){
        propertyName.value = prop.name
        propertyRating.value = if (prop.overallRating == null) "0.0" else getRating(prop.overallRating.overall)
        propertyImageUrl.value = "http://" + prop.images!![0].prefix + prop.images[0].suffix
        propertyLowestPricePerNight.value = calculatePriceInEuros(prop.lowestPricePerNight.value)
        propertyOverview.value = prop.overview
    }

    fun calculatePriceInEuros(lowestPricePerNight: String): String {
        val number = lowestPricePerNight.toDouble() / CONVERTER
        return String.format("%.2f", number) + "€"
    }

    fun getRating(rating : Int):String{
        return (rating.toDouble() / 10 ).toString()
        }

    fun getPropertyName():MutableLiveData<String>{
        return propertyName
    }
    fun getPropertyRating():MutableLiveData<String>{
        return propertyRating
    }
    fun getPropertyImageUrl():MutableLiveData<String>{
        return propertyImageUrl
    }
    fun getPropertyOverview():MutableLiveData<String>{
        return propertyOverview
    }
    fun getPropertyLowestPrice():MutableLiveData<String>{
        return propertyLowestPricePerNight
    }
}
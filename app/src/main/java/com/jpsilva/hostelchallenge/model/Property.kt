package com.jpsilva.hostelchallenge.model

import com.squareup.moshi.Json

data class OverallRating(@Json(name = "numberOfRatings")
                         val numberOfRatings: String = "",
                         @Json(name = "overall")
                         val overall: Int = 0)


data class LowestDormPricePerNight(@Json(name = "currency")
                                   val currency: String = "",
                                   @Json(name = "value")
                                   val value: String = "")


data class FreeCancellation(@Json(name = "description")
                            val description: String = "",
                            @Json(name = "label")
                            val label: String = "")


data class City(@Json(name = "country")
                val country: String = "",
                @Json(name = "name")
                val name: String = "",
                @Json(name = "id")
                val id: Int = 0,
                @Json(name = "idCountry")
                val idCountry: Int = 0)


data class Facilities(@Json(name = "name")
                      val name: String = "",
                      @Json(name = "id")
                      val id: String = "",
                      @Json(name = "facilities")
                      val facilities: List<Facilities>?)


data class LowestPricePerNight(@Json(name = "currency")
                               val currency: String = "",
                               @Json(name = "value")
                               val value: String = "")


data class LowestPrivatePricePerNight(@Json(name = "currency")
                                      val currency: String = "",
                                      @Json(name = "value")
                                      val value: String = "")


data class Pagination(@Json(name = "next")
                      val next: String = "",
                      @Json(name = "numberOfPages")
                      val numberOfPages: Int = 0,
                      @Json(name = "prev")
                      val prev: String = "",
                      @Json(name = "totalNumberOfItems")
                      val totalNumberOfItems: Int = 0)


data class HighestPricePerNight(@Json(name = "currency")
                                val currency: String = "",
                                @Json(name = "value")
                                val value: String = "")


data class Properties(@Json(name = "lowestAverageDormPricePerNight")
                      val lowestAverageDormPricePerNight: Double? = null,
                      @Json(name = "distance")
                      val distance: Distance,
                      @Json(name = "latitude")
                      val latitude: Double = 0.0,
                      @Json(name = "type")
                      val type: String = "",
                      @Json(name = "freeCancellation")
                      val freeCancellation: FreeCancellation,
                      @Json(name = "lowestDormPricePerNight")
                      val lowestDormPricePerNight: LowestDormPricePerNight,
                      @Json(name = "hostelworldRecommends")
                      val hostelworldRecommends: Boolean = false,
                      @Json(name = "freeCancellationAvailableUntil")
                      val freeCancellationAvailableUntil: String = "",
                      @Json(name = "id")
                      val id: Int = 0,
                      @Json(name = "starRating")
                      val starRating: Int = 0,
                      @Json(name = "isFeatured")
                      val isFeatured: Boolean = false,
                      @Json(name = "hwExtra")
                      val hwExtra: String? = null,
                      @Json(name = "lowestAveragePrivatePricePerNight")
                      val lowestAveragePrivatePricePerNight: Double? = null,
                      @Json(name = "longitude")
                      val longitude: Double = 0.0,
                      @Json(name = "overview")
                      val overview: String = "",
                      @Json(name = "images")
                      val images: List<Images>?,
                      @Json(name = "overallRating")
                      val overallRating: OverallRating?,
                      @Json(name = "hbid")
                      val hbid: Int = 0,
                      @Json(name = "address2")
                      val address2: String = "",
                      @Json(name = "address1")
                      val address1: String = "",
                      @Json(name = "isElevate")
                      val isElevate: Boolean = false,
                      @Json(name = "isNew")
                      val isNew: Boolean = false,
                      @Json(name = "lowestAveragePricePerNight")
                      val lowestAveragePricePerNight: Double? = null,
                      @Json(name = "freeCancellationAvailable")
                      val freeCancellationAvailable: Boolean = false,
                      @Json(name = "district")
                      val district: District,
                      @Json(name = "name")
                      val name: String = "",
                      @Json(name = "position")
                      val position: Int = 0,
                      @Json(name = "lowestPricePerNight")
                      val lowestPricePerNight: LowestPricePerNight,
                      @Json(name = "facilities")
                      val facilities: List<Facilities>?,
                      @Json(name = "lowestPrivatePricePerNight")
                      val lowestPrivatePricePerNight: LowestPrivatePricePerNight)


data class Images(@Json(name = "prefix")
                  val prefix: String = "",
                  @Json(name = "suffix")
                  val suffix: String = "")


data class FilterData(@Json(name = "highestPricePerNight")
                      val highestPricePerNight: HighestPricePerNight,
                      @Json(name = "lowestPricePerNight")
                      val lowestPricePerNight: LowestPricePerNight)


data class District(@Json(name = "name")
                    val name: String = "",
                    @Json(name = "id")
                    val id: String = "")


data class Property(@Json(name = "pagination")
                    val pagination: Pagination,
                    @Json(name = "filterData")
                    val filterData: FilterData,
                    @Json(name = "location")
                    val location: Location,
                    @Json(name = "properties")
                    val properties: List<Properties>?)


data class Distance(@Json(name = "units")
                    val units: String = "",
                    @Json(name = "value")
                    val value: Double = 0.0)


data class Location(@Json(name = "city")
                    val city: City,
                    @Json(name = "region")
                    val region: String? = null)


